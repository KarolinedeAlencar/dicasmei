import control from './Control.js';

export default function addActivty() {
    const container = document.querySelector('[data-control="fieldContainer"]');

    const markup = `
        <div class="field">
            <input class="input" type="text" data-js="searchActivity">
            <label class="label">*Atividade Principal</label>
            <ul class="activity-list" data-control="activityList"></ul>
        </div>
    `;

    container.insertAdjacentHTML('beforeend', markup);

    control();
}