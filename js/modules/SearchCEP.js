import insertAdress from './InsertAdress.js';

export default function searchCEP() {
    const cepValue = this.value;
    
    fetch(`https://viacep.com.br/ws/${cepValue}/json/`)
        .then(result => result.json())
        .then(result => insertAdress(result));
}