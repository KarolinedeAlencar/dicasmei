
import fetchTemplate from './FetchTemplate.js';

export default function controlPage() {
    const btnPrev = document.querySelector('[data-control="btnPrev"]');
    const btnNext = document.querySelector('[data-control="btnNext"]');

    const prevPage = btnPrev.getAttribute('data-prev');
    const nextPage = btnNext.getAttribute('data-next');
     
    if (btnPrev) {
        btnPrev.addEventListener('click', function(e) {
            e.preventDefault();
            fetchTemplate(`../steps/${prevPage}.html`);
        });
    }

    if (btnNext) {
        btnNext.addEventListener('submit', function(e) {
            e.preventDefault();
            if (this.classList.contains('cpf--error')) {
                return false;
            } 
            return fetchTemplate(`../steps/${nextPage}.html`);
        });
    }
}