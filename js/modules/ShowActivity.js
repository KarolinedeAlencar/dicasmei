export default function showActivity(matchWords, input) {
    const list = input.nextElementSibling.nextElementSibling;

    if (matchWords.length === 0 || input.value === '') {
        list.innerHTML = '';
    } else {

        const html = matchWords.map(function(item) {
            return `
                <li class="activity-list__item" data-control="activityListItem" data-value="${item}">
                    ${item}
                </li>
            `
        }).join('');
        
        list.innerHTML = html;
    
        const items = Array.from(input.nextElementSibling.nextElementSibling.children);
    
        items.map(function(item) {
            item.addEventListener('click', selectItem);
        });
    
        function selectItem() {
            const selectValue = this.dataset.value;
            input.value = selectValue;
            list.innerHTML = '';
        }
        
    }

}