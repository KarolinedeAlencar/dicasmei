export default function insertAdress(data) {
    const street = document.querySelector('[data-address="street"]');
    const neighborhood = document.querySelector('[data-address="neighborhood"]');
    const state = document.querySelector('[data-address="state"]');
    const city = document.querySelector('[data-address="city"]');
    const complement = document.querySelector('[data-address="complement"]');

    data.logradouro ? street.value = data.logradouro : '';
    data.bairro ? neighborhood.value = data.bairro : '';
    data.uf ? state.value = data.uf : '';
    data.localidade ? city.value = data.localidade : '';
    data.complemento ? complement.value = data.complemento : '';
}