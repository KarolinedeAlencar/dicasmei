import isValid from './CPFValidation.js';
import searchCEP from './SearchCEP.js';
import addActivty from './AddActivty.js';
import controlPage from './ControlPage.js';

import mask from './Mask.js';
import cpfMask from './CPFMask.js';
import rgMask from './RGMask.js';

import showActivity from './ShowActivity.js';

export default function control() {
    const inputCpf = document.querySelector('[data-mask="cpf"]');
    const inputRG = document.querySelector('[data-mask="rg"]');
    const inputCEP = document.querySelector('[data-js="CEP"]');
    const inputAddActivity = document.querySelector('[data-control="addActivity"]');
    const inputSearchActivity = Array.from(document.querySelectorAll('[data-js="searchActivity"]'));
    
    if (inputCpf !== null) {

        inputCpf.addEventListener('keydown', function() {
            mask(this, cpfMask);
        })

        inputCpf.addEventListener('blur', function(e) {
            const cpf = e.target.value.split(/ /)[0].replace(/[^\d]/g, '');

            if(isValid(cpf)) {
                inputCpf.classList.remove('input--error');
                inputCpf.parentElement.parentElement.classList.remove('cpf--error');
                inputCpf.classList.add('input--success');
            } else {
                inputCpf.classList.add('input--error');
                inputCpf.parentElement.parentElement.classList.add('cpf--error');
            }

        });
    }

    if (inputRG) {
        inputRG.addEventListener('keydown', function() {
            mask(this, rgMask);
        });
    }

    if (inputCEP) {
        inputCEP.addEventListener('blur', searchCEP);
    }

    if (inputAddActivity) {
        inputAddActivity.addEventListener('click', addActivty);
    }

    if (inputSearchActivity) {
        inputSearchActivity.map(function(input) {
            input.addEventListener('keyup', function() {
                const inputValue = this.value;
                const regex = new RegExp(inputValue, 'gi');
            
                const activity = [
                    'Internet',
                    'Roupas',
                    'Joias',
                    'Teste'
                ];
            
                const matchWords = activity.filter(function(word) {
                    return word.match(regex);
                });
            
                showActivity(matchWords, this);
               
            })
        })
    }

    controlPage();
}