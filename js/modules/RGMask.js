export default function rgMask(number) {
    number = number.replace(/\D/g,"");
    number = number.replace(/(\d{2})(\d{3})(\d{3})(\d{1})$/,"$1.$2.$3-$4");
    return number;
}