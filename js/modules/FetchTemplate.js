import control from "./Control.js"

export default function fetchTemplate(template) {
    const main = document.querySelector('[data-control="main"]');

    fetch(template)
        .then(function(response) {
            return response.text();
        })
        .then(function(text) {
            main.innerHTML = text;
           control();
        });
}