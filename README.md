# DicasMEI Teste

[Link do Teste](http://dicasmei-teste.netlify.com/)


[Link StyleGuide](http://dicasmei-teste.netlify.com/styleguide)

### Tecnologias Usadas

- JavaScript
- CSS 

Não foi utilizado nenhuma biblioteca ou framework. 
As validações e mascaras foram feitas "na mão", para o layout utilizei conceitos de responsive first (uma atualização do conceito mobile first) e para a estrutura de código, separei 
tanto o CSS quanto o Javascript por módulo para facilitar a manutenção e a organização. Também utilizei o conceito de BEM (Block, Element e Modificator) para organização/nomemclatura do CSS.


Para organizar melhor os componentes e facilitar a padronização, criei também um styleguide, o link se encontra no começo deste documento.

### Alterações

Foram feitas algumas alterações no layout, segue as explicações:


#### Atividades da Empresa

Fiz a alteração para um input de texto, onde quando o usuário começa a escrever a atividade da empresa, aparece as opções, acredito que dessa forma fica melhor a usabilidade do usuário, principalmente no mobile.

#### Trabalha em casa?

No fluxo, o formulário aparece apenas quando clica em alguma opção. Optei por deixar o formulário já aberto, tendo em vista que são os mesmos campos para as duas opções e o usuário já tem conhecimento que é preciso preencher essas informações.

No mobile, optei em deixar da mesma forma.



